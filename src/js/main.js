var timeout = null;
//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js
// ../../node_modules/jquery-mousewheel/jquery.mousewheel.js
//= ../../node_modules/html5shiv/dist/html5shiv.min.js
//= ../../node_modules/svg4everybody/dist/svg4everybody.min.js
//= ../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js
//= ../../node_modules/nouislider/distribute/nouislider.min.js
//= ../../node_modules/aos/dist/aos.js

//= vendors/owl.carousel.js
//= vendors/video/plyr.polyfilled.js
//= vendors/video/plyr.js
//= vendors/wNumb.js

//= vendors/jquery.fancybox.min.js

//= vendors/three.js
//= vendors/inflate.min.js
//= vendors/FBXLoader.js
//= vendors/Detector.js
//= vendors/validate.min.js
//= vendors/imagesloaded.pkgd.min.js
//= vendors/TweenMax.min.js
//= vendors/skrllr.min.js

//= components/advantages.js
//= components/timeline.js
//= components/filter.js
//= components/galleryPage.js
//= components/header.js
//= components/panel.js
//= components/panorama.js
//= components/video.js
//= components/validate.js
//= components/card-carousel.js
//= components/lang.js

//= components/building.js
//= components/hover.js
//= components/scroll.js
//= components/index.js



function tweenTimeline() {
  let el = $('.timeline-years_content')

  if ($('.timeline-years_year') && $('.timeline-years_year').length) {
    var triggerAtY = $('.timeline-years_year').offset().top - $(window).outerHeight();

    function anim() {
      let titles = [...document.querySelectorAll('.timeline-years_point')];
      let years = [...document.querySelectorAll('.timeline-years_year')];

      TweenMax.fromTo('.line', 8, {
        width: '0'
      }, {
        width: '4000px'
      });

      TweenMax.staggerFromTo(years, 1, {
        right: '200%',
        opacity: 0
      }, {
        right: '0',
        opacity: 1,
        ease: Power1.easeInOut,
        delay: .5
      });

      TweenMax.staggerFromTo(titles, 1, {
        opacity: 0,
        scale: 0,
        transform: 'translateX(-300%)'
      }, {
        opacity: 1,
        scale: 1,
        transform: 'translateX(0%)',
        ease: Power2.easeOut,
        delay: 1
      }, .15);
    }

    if ($('.timeline-years_year').offset().top < $(window).outerHeight()) {
      anim();
    } else {
      $(window).scroll(function(event) {
        if (triggerAtY > $(window).scrollTop()) {
          return;
        }
        anim();
        $(this).off(event);
      });
    }

  }
}

$(document).ready(function() {
  tweenTimeline();
  initFullpage();

  $('.js-scrollbar').mCustomScrollbar({
    setTop: 0
  });

  $('.js-scrollbar-horizontal').mCustomScrollbar({
    axis: 'x',
    mouseWheel: {
      enable: false,
      axis: 'x'
    }
  });

  $('.js-menu').on('click', function() {
    $('body').toggleClass('menu-open');
    return false;
  })

  $('.js-orderform').on('click', function() {
    $('body').toggleClass('orderform-open');
    $('.popup-orderform').toggleClass('open');
    return false;
  })

  $('.js-popup-parking').on('click', function() {
    $('body').toggleClass('parking-open');
    $('.popup-parking').toggleClass('open');
    $('.popup-flat').toggleClass('no-scroll');
    return false;
  })

  initCarausels();

  AOS.init({
    duration: 1000,
    once: true
  });

  imagesLoaded(document.querySelectorAll('img'), () => {
    document.body.classList.remove('loading');
  });

  Array.from(document.querySelectorAll('.advantages__item-img')).forEach((el) => {
    const imgs = Array.from(el.querySelectorAll('img'));
    new hoverEffect({
      parent: el,
      intensity: el.dataset.intensity || undefined,
      speedIn: el.dataset.speedin || undefined,
      speedOut: el.dataset.speedout || undefined,
      easing: el.dataset.easing || undefined,
      hover: el.dataset.hover || undefined,
      image1: imgs[0].getAttribute('src'),
      image2: imgs[1].getAttribute('src'),
      img: imgs[0],
      displacementImage: el.dataset.displacement
    });
  });
});

function initCarausels() {
  var $carousels = $('.carousel');

  if ($carousels.length) {
    $carousels.each(function(i, item) {
      var carousel = $(item);
      var owl = carousel.find('.owl-carousel');

      var owlResponsive = owl.data('responsive') ? owl.data('responsive') : {
        0: {
          items: 1
        }
      };

      if ($(carousel).hasClass('carousel-gallery')) {
        owl.owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          nav: false,
          dots: false,
          autoplay: true,
          autoplayHoverPause: true,
          autoplayTimeout: 3000,
          smartSpeed: 500,
          responsive: owlResponsive,
          onInitialized: function(event) {
            initCount(event);
          }
        });
      } else if ($(carousel).hasClass('carousel-50vw')) {
        owl.owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          nav: false,
          dots: false,
          autoplay: true,
          autoplayHoverPause: true,
          autoplayTimeout: 3000,
          smartSpeed: 500,
          responsive: owlResponsive,
          onInitialized: function(event) {
            initCount(event);
          }
        });
      } else {
        owl.owlCarousel({
          loop: true,
          margin: 0,
          responsiveClass: true,
          nav: false,
          dots: false,
          smartSpeed: 500,
          responsive: owlResponsive,
          onInitialized: function(event) {
            initCount(event);
          }
        });
      }

      carousel.find('.owl-carousel_nav_btn-next').click(function() {
        owl.trigger('next.owl.carousel');
      })
      carousel.find('.owl-carousel_nav_btn-prev').click(function() {
        owl.trigger('prev.owl.carousel');
      })

      owl.on('changed.owl.carousel', function(event) {
        initCount(event);
      })

      function initCount(event) {
        var total = event.item.count;
        var item = carousel.find('.owl-carousel_item').eq(event.item.index)

        var current = item.data('index') + 1;

        if (current === 0) {
          current = total;
        }
        if (current > total) {
          current = 1;
        }

        carousel.find('.owl-carousel_nav_count_current').html(current);
        carousel.find('.owl-carousel_nav_count_total').html(total)
      }
    });

  }
}
