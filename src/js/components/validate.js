$(document).ready(function($) {
    var callbackForm = document.querySelector("form#callbackform");

    if(callbackForm) {
        callbackForm.addEventListener("submit", function(ev) {
            ev.preventDefault();
            handleFormSubmit(callbackForm);
        });
    }
        
    
    function handleFormSubmit(form, input) {
        var errors = validate(form, constraints, {fullMessages: false});
        showErrors(form, errors || {});
        if (!errors) {
            showSuccess();
        }
    }

    function showSuccess() {
        console.log("Success!");
    }

    function showErrors(form, errors) {
        $.each(form.querySelectorAll("input[name], select[name]"), function(i, input) {
            showErrorsForInput(input, errors && errors[input.name]);
        });
    }

    function showErrorsForInput(input, errors) {
        var formGroup = closestParent(input.parentNode, "form-controle")
            , messages = formGroup.querySelector(".messages");
        resetFormGroup(formGroup);
        if (errors) {
            formGroup.classList.add("error");
            $.each(errors, function(i, error) {
            addError(messages, error);
            });
        } else {
            formGroup.classList.add("success");
        }
    }

    function closestParent(child, className) {
        if (!child || child == document) {
            return null;
        }
        if (child.classList.contains(className)) {
            return child;
        } else {
            return closestParent(child.parentNode, className);
        }
    }

    function resetFormGroup(formGroup) {
        formGroup.classList.remove("error");
        formGroup.classList.remove("success");
        $.each(formGroup.querySelectorAll(".help-block.error"), function(i, el) {
            el.parentNode.removeChild(el);
        });
        }

    function addError(messages, error) {
        var block = document.createElement("span");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
    }
    
});