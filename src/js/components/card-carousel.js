function initCardCarousel() {
    var self = this;
    var themeCarousels = $('.card-carousel');

    if(!themeCarousels.length) {
        return false;
    }

    self.theme = 'minimal';
    self.carousel = $('.card-carousel[data-theme="' + self.theme + '"]');

    function initCarousel() {
        self.carousel = $('.card-carousel[data-theme="' + self.theme + '"]');

        $(themeCarousels).each(function(i, item) {
            $(item).removeClass('is-active')
        });

        $(self.carousel).addClass('is-active');

        var owl = $('.card-carousel[data-theme="' + self.theme + '"]').find('.owl-carousel');

        owl.owlCarousel({
            loop: true,
            margin: 50,
            responsiveClass: true,
            nav: false,
            dots: false,
            smartSpeed: 500,
            items: 1,
            onInitialized: function(event) {
                initCount(event);
            }
        });

        self.carousel.find('.owl-carousel_nav_btn-next').click(function(e) {
            owl.trigger('next.owl.carousel');
        })
        self.carousel.find('.owl-carousel_nav_btn-prev').click(function() {
            owl.trigger('prev.owl.carousel');
        })

        owl.on('changed.owl.carousel', function(event) {
            initCount(event);
        })
    }

    initCarousel();

    function initCount(event) {
        var total = event.item.count;
        var slide = self.carousel.find('.card-carousel__item').eq(event.item.index)
        var current = slide.data('index') + 1;

        if(current === 0) {
            current = total;
        }
        if(current > total) {
            current = 1;
        }

        carousel.find('.owl-carousel_nav_count_current').html(current);
        carousel.find('.owl-carousel_nav_count_total').html(total);

        if(total === 1) {
            self.carousel.find('.owl-carousel_nav_btn-next').hide()
            self.carousel.find('.owl-carousel_nav_btn-prev').hide()
        }
    }

    $('body').on('click', '.js-flat-themes button', function(e) {
        self.theme = $(this).data('theme');

        $('.js-flat-themes button').each(function(i, item) {
            $(item).removeClass('current')
        });

        if(!$(this).hasClass('current')) {
            $(this).addClass('current');
            initCarousel();
        };

    })
}


$('document').ready(function () {
    initCardCarousel();
})