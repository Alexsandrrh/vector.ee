function Timeline(options) {
    var self = this;

    self.options = {};
    self.orderedData = [];
    self.scrollContentWidth = 0;
    self.canDrag = false;
    self.lastX = 0;

    self.window = $(window);

    self.container = $('.timeline-container');
    self.containerYears = self.container.find('.timeline-years');
    self.containerContent = self.containerYears.find('.timeline-years_content');
    self.points = self.containerYears.find('.js-point');
    self.events = self.containerYears.find('.timeline-years_point');

    self.containerPhoto = self.container.find('.timeline-background');
    self.previewPhoto = self.containerPhoto.find('.js-timeline-pic-preview');
    self.fullPhoto = self.container.find('.js-timeline-pic-fullphoto');

    function setBgImage(image) {
        self.previewPhoto.attr('src', image);
        self.fullPhoto.attr('href', image);
    }

    function drag(e) {
        if (self.canDrag) {
            var canMove = self.scrollContentWidth - Math.floor(self.container.width() * 0.7);
            var maxDragWidth = canMove >= 0 ? canMove : 0;

            var style = self.containerContent.attr("style");

            var findTransform = style.indexOf("translateX");
            var translate = style.substring(findTransform);
            var actualPositionX = Number(translate.replace(/\D/g, ""));

            if (!self.lastX) self.lastX = e.pageX;

            var movedValue = Math.abs(self.lastX) - e.pageX;
            var finalPositionX = Math.abs(actualPositionX + movedValue);
            self.lastX = e.pageX;

            if (actualPositionX + movedValue <= 0) finalPositionX = 0;
            if (actualPositionX + movedValue >= maxDragWidth)
                finalPositionX = maxDragWidth;

            self.containerContent
                .attr("style", `transform: translateX(-${finalPositionX}px)`);
        }
    }

    function initEvents() {
        self.containerContent
            .on('mousemove', function(e) {
                drag(e)
            })
            .on('mousedown', function(e) {
                self.lastX = e.pageX;
                self.canDrag = true;
                self.containerYears.addClass('grabbing');
            })
            .on('mouseup', function(e) {
                self.canDrag = false;
                self.containerYears.removeClass('grabbing');
            })
            .on('mouseleave', function() {
                self.canDrag = false;
            })
            .on('touchmove', function(e) {
                drag(e)
            })
            .on('touchstart', function(e) {
                self.lastX = e.pageX;
                self.canDrag = true;
                self.containerYears.addClass('grabbing');
            })
            .on('touchend', function(e) {
                self.canDrag = false;
                self.containerYears.removeClass('grabbing');
            })
            .on('touchcancel', function() {
                self.canDrag = false;
            })
        
        self.events.on('click', function() {
            var image = $(this).data('image');
            if(image) {
                setBgImage(image)
            }
        })

        self.window.on('resize', function() {
            getOrderedData()
        })
    }

    function getOrderedData() {
        $.each(self.points, function() {
            var $point = $(this);
            var width = Math.floor($point.width());
            
            self.scrollContentWidth += width;
        })
    }

    self.init = function() {
        if(!self.container.length) return false;

            getOrderedData();
            initEvents();
    }
}

$(document).ready(function() {
    var timeline = new Timeline({});

    timeline.init();
})