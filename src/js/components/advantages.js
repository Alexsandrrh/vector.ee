$(document).ready(function() {
    var Advantages = {
		init: function () {
            this.body = $('body');
            this.window = $(window);

            this.advantages = $('.advantages');
            this.advantagesClass = 'advantages';

            this.containerPopup = $('.advantages-popup');
            this.classOpenPopup = 'open';

            this.isOpen = false;

            this.last = '';

            this.coord = {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            };

            this._initEvents();
        },

        _stateBox: function(elem) {
            var self = this;

            var title = $(elem).find('.advantages_title').html();
            var content = $(elem).find('.advantages_text').html();
            var $parent = $('.advantages-popup');
            var boxSizes = {
                width: $(elem).width(),
                height: $(elem).height()
            };

            if (self.isOpen) {
                self.body.addClass('advantage-open')
                self.containerPopup.find('.advantages-popup_title').html(title)
                self.containerPopup.find('.advantages-popup_text').html(content)
                self.containerPopup
                    .addClass(self.classOpenPopup)
                    .mCustomScrollbar({
                        setTop: 0
                    });

                $parent.css({
                    backgroundImage: 'url(' + $(elem).find('img')[0].src + ')'
                });

                $parent.css({
                    top: self.coord.top,
                    left: self.coord.left,
                    width: boxSizes.width
                }).addClass('open');

                TweenMax.to(
                    $($parent), .8, {
                        top: 0,
                        left: 0,
                        width: '100%',
                        ease: Sine.easeInOut,
                        opacity: 1
                    }
                );

                $(this).find('.advantages_text').mCustomScrollbar({
                    setHeight: 500
                });
            } else {
                TweenMax.to(
                    $($parent), .8, {
                        top: self.coord.top,
                        left: self.coord.left,
                        width: boxSizes.width,
                        height: boxSizes.height,
                        ease: Sine.easeInOut,
                        opacity: 0,
                        onCompleteParams: [self],
                        onComplete: function(){
                            self.body.removeClass('advantage-open')
                            self.containerPopup.removeClass(self.classOpenPopup)
                        }
                    }
                )
            }
        },

        _initEvents: function() {
            var self = this;

            $('.js-advantages-open').on('click', function() {
                self.isOpen = true;
                self.coord = {
                    top: $(this).scrollTop(),
                    left: $(this).offset().left
                };
                self.last = this;

                self._stateBox(this);
                return false;
            });

            $('.js-advantages-close').on('click', function() {
                self.isOpen = false;
                self._stateBox(self.last)
                return false;
            })
        },

 	};
    window.Advantages = Advantages;

    Advantages.init();
});
