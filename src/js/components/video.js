$(document).ready(function() {
    var videoPanel = {
		init: function () {
            this.body = $('body');
            this.window = $('window');

            this.container = $('.video-box');
            this.classContainer = 'video-box';

            this.fullVideo = $('.video-box_item');
            this.fullVideoDisplay = $('.video-box_item-display');

            this.activeEmbedId = 'CLhj1BCYYVM';

            this.playerID = this.fullVideoDisplay.attr('id');

            this.plyr_player = null;

            this._initEvents();

            this._initFullVideo();
        },

        _initEvents: function() {
            var self = this;

        },

        _destroyFullVideo: function() {
            var self = this;

            self.plyr_player.destroy();
        },

        _initFullVideo: function() {
            var self = this;

            self.plyr_player = new Plyr('#' + self.playerID);

            var plyr = self.fullVideo;
            var plyrPoster = plyr.data('poster');

            self.plyr_player.on('ready', function() {
                setTimeout(function() {
                    self.plyr_player.poster = plyrPoster;
                }, 500)
            });
        },
 	};
    window.videoPanel = videoPanel;
    
    videoPanel.init();
});