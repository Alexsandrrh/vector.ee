function switchLang() {
    var self = this;

    self.langMain = $('.js-select-lang');
    self.activator = $('.js-lang-btn');
    self.langContainer = $('.js-lang-container');
    self.option = $('.js-lang');
    self.state = false;
    self.temp = $(self.activator).attr('data-lang');
    self.tempLoc = $(self.activator).attr('data-loc');

    $(self.activator).on('click', function(e) {
        e.preventDefault();
        containerState(true);
    });

    function containerState() {
        self.state = !self.state;
        if(!self.state) {
            $(self.activator).removeClass('is-active');
            $(self.langMain).removeClass('is-active');
            $(self.langContainer).removeClass('is-visible');
            return false;
        }
        $(self.activator).addClass('is-active');
        $(self.langMain).addClass('is-active');
        $(self.langContainer).addClass('is-visible');
    }

    $(self.option).on('click', function (e) {
        let lang = $(this).attr('data-lang');
        let loc = $(this).attr('data-loc');
        let temp = self.temp;
        let tempLoc = self.loc;

        $(this).attr('data-lang', temp);
        $(this).find('a').text(temp);
        $(this).find('a').attr('href', tempLoc);

        $(self.activator).attr('data-lang', lang);
        $(self.activator).text(lang);
        $(self.activator).attr('href', '');

        self.temp = lang;
        self.tempLoc = loc;
    });

    $('body').on('click', function(e) {
        if(self.state && !e.target.closest('.js-lang-btn')) {
            containerState();
        }
    });

    window.addEventListener('scroll', function() {
        if(self.state) containerState();
    });
}

$(document).ready(function() {
    switchLang();
})