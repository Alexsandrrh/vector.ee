if($('#map').length) {
    var zoom = $('#map').data('zoom') || 12.5;
    mapboxgl.accessToken = 'pk.eyJ1IjoidGFuYWV2IiwiYSI6ImNqejZ6c29tYzBlajczYnE4aDNsc2xwb2cifQ.scVIhhleg9RrA6wkywzDxQ';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10',
        center: [24.7385176, 59.4220836],
        zoom: zoom
    });

    map.on('load', function() {
        if(dataLocations.length > 0) {
            for(var i = 0; i < dataLocations.length; i++) {
                var point = dataLocations[i];
                var el = document.createElement('div');

                el.className = 'marker';
                el.style.backgroundImage = 'url(' + point.image + ')';
                el.style.width = point.properties.iconSize[0] + 'px';
                el.style.height = point.properties.iconSize[1] + 'px';

                new mapboxgl.Marker(el, {
                        anchor: 'bottom'
                    })
                    .setLngLat(point.coordinates)
                    .addTo(map);
            }

        }
        

        map.addControl(new mapboxgl.NavigationControl());
        map.scrollZoom.disable();
    });
}

