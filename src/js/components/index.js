function initFullpage() {
    var main = document.getElementById('fullpage');
    if(!main) return false;

    var body = document.body;
    body.style.cssText = 'position: fixed; overflow: hidden;';

    var outproject = document.querySelector('.js-panel-outproject');

    const skrllr = new Skrllr('main', {
        transitionTime: 1200,
        container: "main > *",
        easing: 'ease-in-out',
        updateURL: false,
        menu: document.querySelector('.pagination'),
        beforeTransition: (index, nextIndex, next) => before(index, nextIndex, next),
        afterTransition: (index, nextIndex, next) => after(index, nextIndex, next)
    })

    function after(index, nextIndex, next) {
        var darkDetect = next.classList.contains('index-full__section_dark');
        var content = next.querySelector('.index-full__section-content');

        if (darkDetect) {
            outproject.classList.add('dark-bg');
        } else {
            outproject.classList.remove('dark-bg');
        }

        TweenMax.fromTo(content, .75, {
            opacity: 0,
        }, {
            opacity: 1,
            ease: Power2.easeOut
        });
    }

    function before(index, nextIndex, next) {

        var items = [...document.querySelectorAll('.skrllr-section')];
        var content = items[index - 1].querySelector('.index-full__section-content');

        TweenMax.fromTo(content, .75, {
            opacity: 1,
        }, {
            opacity: 0,
        });

    }
}