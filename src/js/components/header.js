function Header(options) {
    var self = this;

    self.window = $(window);

    self.options = {};
    self.lastY = self.window.scrollTop();

    self.container = $('.header');



    self.sections = $('.section');

    self.classNotVisible = 'notvisible';

    self.sectionsPositions = [];

    self.lastScrollTop = 0;

    function sections() {
        self.sections.each(function(i, item) {
            var $section = $(this);
            var sectionOffsetTop = Math.ceil($section.offset().top + (self.container.height() / 2));
            var sectionOffsetBottom = Math.ceil(sectionOffsetTop + $section.height());

            if($section.height() > 0) {
                self.sectionsPositions.push({
                    offsetTop: sectionOffsetTop,
                    offsetBottom: sectionOffsetBottom,
                    backgroundColor: $section.css('backgroundColor')
                })
            }
        })

        sectionsInView();
    }

    function sectionsInView() {
        var visibleSection = self.sectionsPositions.filter(function (n, i) {
            return self.lastY >= n.offsetTop;
        });
        if(!self.sectionsPositions[0]) return false;
        if(!visibleSection[visibleSection.length - 1]) {
            changeColor(self.sectionsPositions[0].backgroundColor)
        }else {
            changeColor(visibleSection[visibleSection.length - 1].backgroundColor)
        }

    }

    function changeColor(color) {
        var color = color.replace(/[^0-9,^,]/g, '').split(',');
        r = parseInt(color[0]);
        g = parseInt(color[1]);
        b = parseInt(color[2]);

        var light = (1 - (0.299 * r + 0.587 * g + 0.114 * b) / 255 < 0.5)

        if(!light) {
            self.container.addClass('dark-bg')
        }else {
            self.container.removeClass('dark-bg')
        }
    }

    function setVisible(delta) {
        if(delta < 0) {
            self.container.addClass(self.classNotVisible)
        }else {
            self.container.removeClass(self.classNotVisible)
        }
    }


    var c, currentScrollTop = 0,
        navbar = $('.header');
    function setVisibleClass() {
        self.lastY = self.window.scrollTop();

        if (self.lastY > self.lastScrollTop){
            self.container.removeClass('top')
        }else if (self.lastY < 40) {
            self.container.addClass('top')
        } else {
        }

        var a = $(window).scrollTop();
        var b = navbar.height();

        currentScrollTop = a;

        if (c < currentScrollTop && a > b + b) {
            navbar.addClass(self.classNotVisible);
        } else if (self.lastY < 40){
            self.container.addClass('top')
        } else if (c > currentScrollTop && !(a <= b)) {
            navbar.removeClass(self.classNotVisible);
        }
        c = currentScrollTop;

        self.lastScrollTop = self.lastY;
    }

    function initEvents() {
        self.window.on('scroll', function(event){
            setVisibleClass();

            sectionsInView();
        }).on('resize', function() {
            sections();
        });

        self.window.on('mousewheel DOMMouseScroll MozMousePixelScroll touchmove', function(event) {
            // delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail || event.originalEvent.changedTouches[0].clientY);

            // setVisible(delta);
        });
    }

    function hideOnScroll() {
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            var headerHeight = document.querySelector(".header").clientHeight;
            console.log(headerHeight)
            if (prevScrollpos > currentScrollPos) {
                document.querySelector(".header").style.top = '0px';
            } else {
                document.querySelector(".header").style.top = '-' + headerHeight + 'px';
            }
            prevScrollpos = currentScrollPos;
        }
    }

    self.init = function() {
        if(!self.container.length) return false;
        // if(self.container.hasClass('header_index')) return false;

        initEvents();

        setVisibleClass();

        // hideOnScroll();

        setTimeout(function() {
            sections();
        }, 0)
    }
}

$(document).ready(function() {
    var header = new Header({});

    header.init();
})
