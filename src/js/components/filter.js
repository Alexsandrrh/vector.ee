function Filter(options) {
    var self = this;

    self.options = {};

    self.apiFlats = 'http://vector.cn35744.tmweb.ru/local/tools/flats.php';
    self.apiFloors = 'http://vector.cn35744.tmweb.ru/local/tools/floor.php';
    self.apiBuro = 'http://vector.cn35744.tmweb.ru/local/tools/floor_buro.php';

    self.selectedFloor = {
        id: null,
        tower: null,
        floor: null
    };

    self.window = $(window);

    self.container = $('.filter');
    self.containerViews = $('.filter-views');
    self.viewsItem = self.containerViews.find('.filter-views_item');

    self.containerFlatsList = $('.flat-list-data');
    self.containerFlat = $('.popup-flat');

    self.switchView = $('.filter-view-switch');
    self.switchTower = $('.js-towers');
    self.switchRooms = $('input[name="flats"]');

    self.buttonBuroSelectFloor = $('.js-building-buro-floor');
    self.buttonSelectFloor = $('.js-building-floor');

    self.buttonGetPdf = $('.js-get-pdf');

    self.flatSchemeInFloor = $('.js-floor-flat');

    self.totalAmount = $('.js-filter-amount');

    self.floorUpBtn = $('.js-floor-up');
    self.floorDownBtn = $('.js-floor-down');

    self.defaultPlanId = $('.js-building-floor[data-id="1_5"]').attr('id');



    var json = (function() {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': '../config/flats.json',
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();

    self.superTower = {
        aInternal: 1,
        aListener: function(val) {},
        set a(val) {
            this.aInternal = val;
            this.aListener(val);
        },
        get a() {
            return this.aInternal;
        },
        registerListener: function(listener) {
            this.aListener = listener;
        }
    };

    self.superTower.registerListener(function(val) {

        self.switchTower.each(function(i, item){
            if(val == 1) {
                self.switchTower[0].classList.add('checked');
                self.switchTower[1].classList.remove('checked');
                $('.js-floor-total').text('16')
            } else if (val == 2) {
                self.switchTower[1].classList.add('checked');
                self.switchTower[0].classList.remove('checked')
                $('.js-floor-total').text('12')
            } else if (val.length === 2) {
                self.switchTower[0].classList.add('checked');
                self.switchTower[1].classList.add('checked');
                $('.js-floor-total').text('16')
            } else {
                self.switchTower[0].classList.remove('checked');
                self.switchTower[1].classList.remove('checked');
            }

        })
    });

    self.flatsList = json;

    self.filterForm = self.container.find('form');
    self.filterButtonReset = $('.js-filter-reset');

    self.filterSortButton = $('.js-filter-sort');
    self.filterSort = 'no';
    self.filterSortDir = 'desc'; // asc / desc

    self.floorPlanImg = $('.js-floorplan');
    self.floorPlanMaskesImg = $('.js-floorplan-maskes');

    self.isOpenFilter = false;
    self.isOpenFlat = false;
    self.isOpenFlatId = false;
    self.isOpenView = 0; // list / plan
    self.isView = ['list', 'plan'];
    self.isViewTower = 1;
    self.isPage = self.filterForm.data('page');

    self.filterParams = filterParams;

    self.filter = {
        flats: [],
        towers: [],
        floor: [],
        price: [],
        rooms: []
    };

    self.moneyFormat = wNumb({
        mark: '.',
        thousand: ' ',
        prefix: '',
        suffix: ''
    });

    self.rangeSliders = $('.js-range-slider');

    self.classOpen = 'open';
    self.classView = 'show';

    self.defaultPlanId = (self.isPage === 'buro' ? 25 : $('.js-building-floor[data-id="1_5"]').attr('id')) ;


    function findPlanId(i) {
        return $('.js-building-floor[data-id="'+i+'"]').attr('id');
    }

    function stateFilter() {
        if(self.isOpenFilter) {
            self.container.addClass(self.classOpen)
        }else {
            self.container.removeClass(self.classOpen)
        }
    }

    function stateFlat() {
        if(self.isOpenFlat) {
            self.containerFlat.addClass(self.classOpen)
            $('body').addClass('no-scroll');

        }else {
            self.containerFlat.removeClass(self.classOpen)
            $('body').removeClass('no-scroll');
            $('.custom-pan__vector').removeClass('come-in')

        }
    }

    function stateViews() {
        self.viewsItem.removeClass(self.classView);
        self.containerViews.find('.filter-views_item[data-type="' + self.isView[self.isOpenView] + '"]').addClass(self.classView);
        ruleTableHead();
    }

    function setFilterParams() {
        var params = self.filterParams[self.isPage].towers;

        var floorsSlider = $('#floors-slider');
        var priceSlider = $('#price-slider');

        self.filterParams[self.isPage].towers.price = setFilter('price');
        self.filterParams[self.isPage].towers.floor = setFilter('floor');

        if(params.floors[0]) {

            // getFloorPlan();
        }


        floorsSlider
            .data('rangemin', params.floors[0])
            .data('rangemax', params.floors[1]);

        self.filter.floor = params.floors;

        priceSlider
            .data('rangemin', params.price[0])
            .data('rangemax', params.price[1]);

        self.filter.price = params.price;

        self.switchTower.each(function() {
            $(this).prop('checked', true);
        });
        self.filter.towers = params.towers;
        self.superTower.a = self.filter.towers;

        self.switchRooms.each(function() {
            $(this).prop('checked', true);
        });
        self.filter.flats = params.flats;

        setTimeout(function() {
            createUiSlider();

            filterFlatsList();
        }, 0);
    }

    function destroyUiSlider() {
        $.each(self.rangeSliders, function() {
            var $elem = $(this);
            var $slider = document.getElementById($elem.attr('id'));

            $slider.noUiSlider.destroy();
        })
    }

    function createUiSlider() {
        $.each(self.rangeSliders, function() {
            var $elem = $(this);
            var $slider = document.getElementById($elem.attr('id'));
            var rangeMin = $elem.data('rangemin') ? $elem.data('rangemin') : 0;
            var rangeMax = $elem.data('rangemax') ? $elem.data('rangemax') : 1;

            noUiSlider.create($slider, {
                start: [rangeMin, rangeMax],
                connect: true,
                tooltips: true,
                range: {
                    'min': rangeMin,
                    'max': rangeMax
                },
                format: wNumb({
                    thousand: ' ',
                    decimals: 0,
                    prefix: $elem.data('prefix') ? $elem.data('prefix') : '',
                    suffix: $elem.data('suffix') ? $elem.data('suffix') : '',
                }),
                step: $elem.data('step') ? $elem.data('step') : 1,
            }).on('change', function(values, handle, value) {
                self.filter[$elem.data('param')] = value;

                filterFlatsList()
            });
        })
    }
    let ruleTableHead = function () {
        const ths = [...document.querySelectorAll('.fix-head thead th')];
        const tds = [...document.querySelectorAll('.fix-head tbody tr:first-child td')].map((item) => {
            return item.offsetWidth;
        });

        for (let i = 0, j = 0; i < ths.length; i++) {
            j = i;
            ths[i].style.width = tds[j] + 'px';
        }

    };
    function setFilter(filterType) {
        let data = self.flatsList[self.isPage];

        let minType = (() => {
            return data.reduce((min, p) => parseInt(p[filterType]) < min ? parseInt(p[filterType]) : min, parseInt(data[0][filterType]));
        })();
        let maxType = (() => {
            return data.reduce((max, p) => parseInt(p[filterType]) > max ? parseInt(p[filterType]) : max, parseInt(data[0][filterType]));
        })();


        return [minType, maxType];
    }

    function filterFlatsList() {
        var flatsList = [];
        var floorsPlans = $('.tower-floor');

        floorsPlans.each(function() {
            var params = $(this).prop('id').split('-');

            if(self.filter.towers.indexOf(params[0]) != -1 && (params[1] >= self.filter.floor[0] && params[1] <= self.filter.floor[1])) {
                $(this).find('rect').removeClass('is-hidden');
            }else {
                $(this).find('rect').addClass('is-hidden');
            }
        })

        flatsList = self.flatsList[self.isPage].sort(function(a, b) {
            var a = parseInt(a[self.filterSort]);
            var b = parseInt(b[self.filterSort]);

            switch (self.filterSortDir) {
                case 'desc':
                        return (parseInt(a) - parseInt(b));
                break;
                case 'asc':
                        return (parseInt(b) - parseInt(a));
                break;
                default:

                break;
            }
        }).filter(function(flat) {
            return self.filter.towers.indexOf(flat.tower) !== -1
                && (flat.floor >= self.filter.floor[0] && flat.floor <= self.filter.floor[1])
                && (self.filter.flats.indexOf(flat.rooms) !== -1)
                && (flat.price >= Math.ceil(self.filter.price[0]) && flat.price < Math.ceil(self.filter.price[1]));
        });

        var type = self.containerFlatsList.data('type');
        self.totalAmount.html(flatsList.length)
        setFlatsList(flatsList, type);
    }

    function setFlatsList(flatsList, type) {
        if(!flatsList.length) {
            // return false;
        }
        self.containerFlatsList.html('');
        if(type == 'buro') {
            for (var index = 0; index < flatsList.length; index++) {
                const $flat = flatsList[index];

                self.containerFlatsList.append('<tr id="' + $flat.id + '"  data-floor="'+ $flat.floor + '" data-tower="'+ $flat.tower + '" class="flat-list-data_row js-flat">'
                    +'<td data-name="Tower" class="flat-list-data_col fl-tower">' + $flat.tower + '</td>'
                    +'<td data-name="Floor" class="flat-list-data_col fl-floor">' + $flat.floor + '</td>'
                    +'<td data-name="No." class="flat-list-data_col fl-no">' + $flat.number + '</td>'
                    +'<td data-name="Rooms" class="flat-list-data_col fl-rooms">' + $flat.rooms + '</td>'
                    +'<td data-name="Square" class="flat-list-data_col fl-square">' + $flat.square + 'm²</td>'
                    +'<td data-name="Price" class="flat-list-data_col fl-price">' + self.moneyFormat.to(parseInt($flat.price)) + '</td>'
                    +'<td data-name="Status" class="flat-list-data_col fl-view">' + ($flat.view ? $flat.view : '') + '</td>'
                    +'</tr>')
            }
        } else {
            for (var index = 0; index < flatsList.length; index++) {
                const $flat = flatsList[index];
                self.containerFlatsList.append('<tr id="' + $flat.id + '"  data-floor="'+ $flat.floor + '" data-tower="'+ $flat.tower + '" class="flat-list-data_row js-flat">'
                    +'<td data-name="Tower" class="flat-list-data_col fl-tower">' + $flat.tower + '</td>'
                    +'<td data-name="Floor" class="flat-list-data_col fl-floor">' + $flat.floor + '</td>'
                    +'<td data-name="No." class="flat-list-data_col fl-no">' + $flat.number + '</td>'
                    +'<td data-name="Rooms" class="flat-list-data_col fl-rooms">' + $flat.rooms + '</td>'
                    +'<td data-name="Square" class="flat-list-data_col fl-square">' + $flat.square + 'm²</td>'
                    +'<td data-name="Balcony" class="flat-list-data_col fl-balcony">' + ($flat.balcony == null ? '—' : $flat.balcony + 'm²') + '</td>'
                    +'<td data-name="Price" class="flat-list-data_col fl-price">' + self.moneyFormat.to(parseInt($flat.price)) + '</td>'
                    +'<td data-name="View" class="flat-list-data_col fl-view">' + ($flat.view ? $flat.view : '') + '</td>'
                    +'</tr>')
            }
        }

        $('.js-flat').each(function(i, item){
            $(item).on('click', function(){
                var id = $(this)[0].id;
                var dataTower = $(this)[0].dataset.tower;
                var dataFloor = $(this)[0].dataset.floor;
                var dataId = dataTower + '_' + dataFloor;
                filter.openFlat(id, dataId, dataTower, dataFloor);
            })
        });



        ruleTableHead();

        window.addEventListener('resize', ruleTableHead);

        const h = document.querySelector('.fix-head thead');
        let save;

        function fixT() {
            if(window.innerWidth < 1200) return;
            if(document.querySelector('tbody').clientHeight < window.innerHeight) return;

            const b = document.querySelector('.fix-head tbody tr:last-child');
            const topB = b.offsetTop;
            const topT = document.querySelector('.flat-list').getBoundingClientRect().top + document.documentElement.scrollTop;

            if(window.scrollY === topT) {
                save = window.scrollY;
            }
            h.style.width = document.querySelector('.flat-list').clientWidth + 'px';

            if(window.scrollY >= topT) {
                h.classList.add('fixed');
                if(window.scrollY <= (topB + window.innerHeight - 180 + 62)) {
                    h.classList.add('fixed');
                    h.classList.remove('last-visible');
                } else {
                    h.classList.remove('fixed');
                    h.classList.add('last-visible');
                }
            } else {
                h.classList.remove('fixed');
                h.classList.remove('last-visible');
                h.offsetTop = save;
            }
        }

        window.addEventListener('scroll', fixT);

    }

    function parseContent(elem, data) {
        var prefix = elem.data('prefix') || '';
        var suffix = elem.data('suffix') || '';
        var type = elem.data('type') || 'text';
        var content = '';

        switch(type) {
            case 'price':
                content = prefix + self.moneyFormat.to(parseInt(data)) + suffix;

                elem.html(content);
            break;
            case 'img':
                content = data;

                elem.attr('src', content);
            break;
            default:
                content = prefix + data + suffix;

                elem.html(content);
            break;
        }
    }

    function setFlatInfo(data) {
        var dataKeys = Object.keys(data);

        $.each(dataKeys, function(index, key) {
            var elem = self.containerFlat.find('.js-flatinfo-' + key);
            if(key === 'id') {
                $('.js-get-pdf').attr('href', 'http://vector.cn35744.tmweb.ru/local/tools/pdf2.php?PID=' + data[key] + '&LANG=ru')
            }

            if(key === 'view') {
                let img = data[key];
                if(img === 'Lõuna') img = 'louna';
                if(img === 'Järv') img = 'jarv';
                $('.js-flat-panorama').attr('style', 'background-image: url("/local/templates/main/static/img/' + img.toLowerCase() + '.svg")');
            }

            if(key === 'view_img') {
                $(elem[0]).css({
                    'background-image': 'url(http://vector.cn35744.tmweb.ru'+ data[key] +')',
                })
            } else if(key === "svg_detail") {
                $(elem[0]).attr("src", 'http://vector.cn35744.tmweb.ru'+ data[key]);
            } else {
                parseContent(elem, data[key]);
            }
        });

        stateFlat()
    }

    function getFlatInfo(id) {

        _request(self.apiFlats, {ID: id}, function (callback) {
            setFlatInfo(callback)
        }, function (errback) {
            console.error(errback, id)
        });

    }

    function getPdf(id, lang) {
        _request('http://vector.cn35744.tmweb.ru/local/tools/pdf2.php', {PID: id, LANG: lang});
    }

    function getFloorId(dataId) {
        return $('.js-building-floor[data-id="' + dataId + '"]').attr('id')
    }

    function setFloorPlan(data) {
        self.floorPlanMaskesImg.html('');
        self.floorPlanImg.attr('src', 'http://vector.cn35744.tmweb.ru' + data.svg_floor);

        var content = '';


        if(data.flats && data.flats.length) {
            for (var i = 0; i < data.flats.length; i++) {
                var flatData = data.flats[i];
                var id = flatData.id;
                var mask = flatData.svg_mask_points != null ? flatData.svg_mask_points : '0';

                content = content + '<polygon class="floor-flat js-floor-flat" id="' + flatData.id + '" data-tower="' + data.tower + '" data-floor="' + data.floor + '" serif:id="' + flatData.id + '" points="'+ mask + '" style="fill:rgb(228,142,60);"></polygon>'
            }
        }

        self.floorPlanMaskesImg.html(content);

        $('.js-floor-flat').each(function(i, item){
            $(item).on('click', function(){
                var id = $(this)[0].id;
                var dataTower = $(this)[0].dataset.tower;
                var dataFloor = $(this)[0].dataset.floor;
                var dataId = dataTower + '_' + dataFloor;
                filter.openFlat(id, dataId, dataTower, dataFloor);
            })
        });

        stateViews();

    }

    function getFloorPlan(id) {
        if(self.isPage === 'flat') {
            _request(self.apiFloors, {ID: id}, function (callback) {
                // обрабатываем результат
                setFloorPlan(callback);
            }, function (errback) {
                // обрабатываем ошибки
            });
        } else {
            _request(self.apiBuro, {ID: id}, function (callback) {
                // обрабатываем результат
                setFloorPlan(callback);
            }, function (errback) {
                // обрабатываем ошибки
                console.error(errback)
            });
        }

    }

    function _request(url, data, callback, errback) {
        var querystring = _querystring(data);

        var url = url + '?' + querystring;

        var status = '';

        var xhr = new XMLHttpRequest();

        xhr.open('GET', url, true);

        xhr.onreadystatechange = function() {
          if (this.readyState != 4) return;

          if (this.status != 200) {
              status = 'ошибка: ' + (this.status ? this.statusText : 'запрос не удался');
              errback && errback(status);
          } else {
              data = JSON.parse(xhr.responseText);
              callback && callback(data);
          }
        };

        xhr.send();
    }

    function _querystring(data) {
        var string;
        var arr = [];
        if(typeof data == 'object') {
            $.each(data, function(i, item) {
                if(typeof item == 'object'){
                    for (var k = item.length - 1; k >= 0; k--) {
                        if(item[k] != ''){
                            arr.push(i + '=' + item[k]);
                        }
                    }
                }else{
                    arr.push(i + '=' + item);
                }
            });
        }

        string = arr.join('&');

        return string;
    }

    function initEvents() {
        self.switchView.on('change', function() {
            if($(this).prop('checked')) {
                self.isOpenView = 1;
            }else {
                self.isOpenView = 0;
            }

            stateViews()

            return false;
        })

        self.switchTower.on('change', function() {
            var value = self.filter.towers;

            if(!$(this).hasClass('checked')) {
                $(this).addClass('checked');
                $('input.js-towers[value="' + $(this).val() + '"]').addClass('checked')
                value.push($(this).val())
            } else {
                $(this).removeClass('checked');
                $('input.js-towers[value="' + $(this).val() + '"]').removeClass('checked')
                value.splice(value.indexOf($(this).val()), 1);
            }
            value = value.filter(function(item, index) {
                return value.indexOf(item) >= index;
            }).sort();


            self.filter.towers = value;
            self.superTower.a = value;

            filterFlatsList();

            return false;
        });

        self.switchRooms.on('change', function() {
            var value = [];

            $.each(self.switchRooms, function(i, item) {
                if($(item).prop('checked')) {
                    let val = $(item).val();
                    // if(value.indexOf(val)) {
                        value.push(val)
                    // }
                }
            });

            self.filter.flats = value;

            filterFlatsList();

            return false;
        });

        self.window.on('resize', function() {
            if($(this).width() < 1600) {
                self.isOpenView = 0;
                self.switchView.prop('checked', false)

                stateViews()
            }
        });

        // $(document).on('click', self.flatSchemeInFloor, function() {
        //     var id = $(this).attr('id');

        //     if(!id) return false;

        //     self.openFlat($(this).attr('id'));

        //     return false;
        // })

        self.buttonBuroSelectFloor.on('click', function() {
            var params = $(this).attr('data-id').split('_');
            var id = $(this)[0].id;

            var $slider = document.getElementById('floors-slider');

            $slider.noUiSlider.updateOptions({
                start: [params[1], params[1]],
            });
            self.filter.floor = [params[1],params[1]]

            filterFlatsList();
            getFloorPlan(id);
            scrollTo('#filter-views');

            return false;
        });

        self.buttonSelectFloor.on('click', function() {
            var params = $(this).attr('data-id').split('_');
            var $slider = document.getElementById('floors-slider');
            var id = $(this)[0].id;

            $slider.noUiSlider.updateOptions({
                start: [params[1], params[1]],
            });

            $('input[name="towers"]').each(function() {
                if($(this).val() != params[0]) {
                    $(this).trigger('click')
                }
            });

            self.filter.towers = [params[0]];
            self.filter.floor = [params[1],params[1]];

            self.superTower.a = self.filter.towers;

            $('.js-floor-current').text(params[1]);

            getFloorPlan(id);

            filterFlatsList();

            scrollTo('#filter-views');

            return false;
        });

        self.filterSortButton.on('click', function() {
            self.filterSort = $(this).data('sort');
            if(self.filterSortDir == 'asc') {
                self.filterSortDir = 'desc';

            }else {
                self.filterSortDir = 'asc';
            }

            $(this).attr('data-sortdir', self.filterSortDir);

            if($('.is-active[data-sort="' + self.filterSort + '"]')) {
                self.filterSortButton.each(function(i, btn) {
                    $(btn).removeClass('is-active');
                });
            }

            $('[data-sort="' + self.filterSort + '"]').addClass('is-active');

            filterFlatsList()

            return false;
        });

        self.filterButtonReset.on('click', function() {
            destroyUiSlider();

            setFilterParams();

            return false;
        });

        self.floorUpBtn.on('click', function() {

            if(self.filter.floor[0] < (self.filter.towers[0] === '2' ? 12 : 16)) {
                self.filter.floor = [++self.filter.floor[0], ++self.filter.floor[1]];

                $('.js-floor-current').text(self.filter.floor[0]);
                var $slider = document.getElementById('floors-slider');
                $slider.noUiSlider.updateOptions({
                    start: [self.filter.floor[0], self.filter.floor[0]],
                });
                var newId = findPlanId(self.filter.towers[0] + '_' + self.filter.floor[0]);
                getFloorPlan(newId);
                filterFlatsList();

            }
        })

        self.floorDownBtn.on('click', function() {

            if(self.filter.floor[0] > 5) {
                self.filter.floor = [--self.filter.floor[0], --self.filter.floor[1]];
                $('.js-floor-current').text(self.filter.floor[0]);
                var $slider = document.getElementById('floors-slider');
                $slider.noUiSlider.updateOptions({
                    start: [self.filter.floor[0], self.filter.floor[0]],
                });

                var newId = findPlanId(self.filter.towers[0] + '_' + self.filter.floor[0]);
                getFloorPlan(newId);
                filterFlatsList()
            }
        })
    }

    function scrollTo(hash) {
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 1000);
    }

    function clearSelection() {
        if(document.selection && document.selection.empty) {
            document.selection.empty();
        } else if(window.getSelection) {
            var sel = window.getSelection();
            sel.removeAllRanges();
        }
    }

    self.init = function() {
        if(!self.container.length) return false;

        setFilterParams();
        initEvents();
        getFloorPlan(self.defaultPlanId);
        $('.js-floor-current').text(self.filter.floor[0]);
        clearSelection();


        const f = document.querySelector('.flat-filter');
        const topF = f.offsetTop;
        let save = window.scrollY;
        function fixF() {
            if(window.innerWidth < 1200) return;

            const a = document.querySelector('.about-container');
            const topA = a.offsetTop;
            if(window.scrollY === topF) {
                save = window.scrollY;
            }

            if(window.scrollY >= topF) {
                f.classList.add('fixed');
                if(window.scrollY <= (topA - f.clientHeight) ) {
                    f.classList.add('fixed');
                    f.classList.remove('to-bottom');
                } else {
                    f.classList.remove('fixed');
                    f.classList.add('to-bottom');
                }
            } else {
                f.classList.remove('fixed');
                f.classList.remove('to-bottom');
                f.offsetTop = save;
            }
        }

        window.addEventListener('scroll', fixF);


    }

    self.openFilter = function() {
        self.isOpenFilter = true;
        $('body').addClass('no-scroll')
        stateFilter()
    };

    self.hideFilter = function() {
        self.isOpenFilter = false;
        $('body').removeClass('no-scroll')
        stateFilter()
    };

    self.openFlat = function(id, dataId, newTower, newFloor) {

        var floorId = getFloorId(dataId);
        getFloorPlan(floorId);

        self.isOpenFlat = true;

        $('.popup-flat').addClass('open');
        $('body').addClass('no-scroll');

        getFlatInfo(id);
        $('.custom-pan__vector').removeClass('come-in');

        if(window.history&&window.history.pushState) {
            window.history.pushState(null, null, '');
            $(window).on('popstate', function() {
                    self.hideFlat();
                }
            )
        }

        self.superTower.a = newTower;
        self.filter.towers = [newTower];
        self.filter.floor = [newFloor, newFloor];
        filterFlatsList();
        var $slider = document.getElementById('floors-slider');
        $('.js-floor-current').text(self.filter.floor[0]);

        $slider.noUiSlider.updateOptions({
            start: [newFloor, newFloor],
        });
    };

    self.hideFlat = function() {
        self.isOpenFlat = false;
        $('.popup-flat').removeClass('open')
        $('body').removeClass('no-scroll')
        $('.custom-pan__vector').removeClass('come-in')

        stateFlat()
    };
}

var filter = new Filter({});

$(document).ready(function() {
    filter.init();

    var body = document.body,
        timer;

    window.addEventListener('scroll', function() {
        clearTimeout(timer);
        if(!body.classList.contains('disable-hover')) {
            body.classList.add('disable-hover')
        }

        timer = setTimeout(function(){
            body.classList.remove('disable-hover')
        }, 200);
    }, false);
})
