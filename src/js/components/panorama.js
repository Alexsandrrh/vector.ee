function Panorama(options) {
    var self = this;

    self.options = {};
    self.scrollContentWidth = 0;
    self.canDrag = false;
    self.lastX = 0

    self.window = $(window);

    self.container = $('.panorama');
    self.containerContent = self.container.find('.panorama-content');

    function drag(e) {
        if (self.canDrag) {
            var canMove = self.scrollContentWidth - Math.floor(self.container.width() * 1);
            var maxDragWidth = canMove >= 0 ? canMove : 0;

            var style = self.containerContent.attr("style");

            var findTransform = style.indexOf("translateX");
            var translate = style.substring(findTransform);
            var actualPositionX = Number(translate.replace(/\D/g, ""));

            if (!self.lastX) self.lastX = e.pageX;

            var movedValue = Math.abs(self.lastX) - e.pageX;
            var finalPositionX = Math.abs(actualPositionX + movedValue);
            self.lastX = e.pageX;

            if (actualPositionX + movedValue <= 0) finalPositionX = 0;
            if (actualPositionX + movedValue >= maxDragWidth)
                finalPositionX = maxDragWidth;

            self.containerContent
                .attr("style", `transform: translateX(-${finalPositionX}px)`);
        }
    }

    function initEvents() {
        self.container
            .on('mousemove', function(e) {
                drag(e)
            })
            .on('mousedown', function(e) {
                self.lastX = e.pageX;
                self.canDrag = true;
                self.container.addClass('grabbing');
            })
            .on('mouseup', function(e) {
                self.canDrag = false;
                self.container.removeClass('grabbing');
            })
            .on('mouseleave', function() {
                self.canDrag = false;
            })
            .on('touchmove', function(e) {
                drag(e)
            })
            .on('touchstart', function(e) {
                self.lastX = e.pageX;
                self.canDrag = true;
                self.container.addClass('grabbing');
            })
            .on('touchend', function(e) {
                self.canDrag = false;
                self.container.removeClass('grabbing');
            })
            .on('touchcancel', function() {
                self.canDrag = false;
            })

        self.window.on('resize', function() {
            getOrderedData()
        })
    }

    function getOrderedData() {
        var width = Math.floor(self.containerContent.width());

        self.scrollContentWidth += width;
    }

    self.init = function() {
        if(!self.container.length) return false;
        getOrderedData();
        initEvents();
    }
}

/*function parallax(event) {
    var $slider = $(".custom-pan__vector");
    var panoramaTop = document.querySelector('.custom-pan').offsetTop;
    var scroll = document.querySelector('.popup-flat').scrollTop;
    var scaleFactor = $slider.data('speed');
    var scale, opacity;

    if(scroll < panoramaTop) {
        scale = (1 + (scroll * scaleFactor / 500) * 10);
        opacity = 1 - (scroll * scaleFactor * 3) / 100;
    }

    $slider.css({
        'transform': 'scale(' + scale + ', ' + scale + ')',
        'opacity': opacity
    });
}*/

$('.popup-flat').on("scroll mousewheel DOMMouseScroll", function(event) {

    $(".custom-pan").each(function(i, el) {
        var el = $('.custom-pan__vector');
        if (el.visible(true)) {
            el.addClass("come-in");
        }
    });

});



$(document).ready(function() {
    var panorama = new Panorama({});

    // panorama.init();
})