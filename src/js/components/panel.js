function Panel(options) {
    var self = this;

    self.window = $(window);

    self.options = {};
    self.lastY = self.window.scrollTop();

    self.container = $('.panel');
    self.containerOutproject = $('.panel-outproject');
    self.sections = $('.section');

    self.buttonOutproject = $('.js-panel-outproject');

    self.openOutproject = false;

    self.sectionsPositions = [];

    self.classOpen = 'open';

    function stateOutproject() {
        if(self.openOutproject) {
            self.containerOutproject.addClass(self.classOpen)
        }else {
            self.containerOutproject.removeClass(self.classOpen)
        }
    }

    function sections() {
        self.sections.each(function(i, item) {
            var $section = $(this);
            var sectionOffsetTop = Math.ceil($section.offset().top + (self.container.height() / 2));
            var sectionOffsetBottom = Math.ceil(sectionOffsetTop + $section.height());

            if($section.height() > 0) {
                self.sectionsPositions.push({
                    offsetTop: sectionOffsetTop,
                    offsetBottom: sectionOffsetBottom,
                    backgroundColor: $section.css('backgroundColor')
                })
            }
        })

        sectionsInView();
    }

    function sectionsInView() {
        var visibleSection = self.sectionsPositions.filter(function (n, i) {
            return self.lastY >= n.offsetTop - self.window.height();
        })
        if(!self.sectionsPositions[0]) return false;
        if(!visibleSection[visibleSection.length - 1]) {
            changeColor(self.sectionsPositions[0].backgroundColor)
        }else {
            changeColor(visibleSection[visibleSection.length - 1].backgroundColor)
        }
        
    }
    
    function changeColor(color) {
        var color = color.replace(/[^0-9,^,]/g, '').split(',');
        r = parseInt(color[0]);
        g = parseInt(color[1]);
        b = parseInt(color[2]);

        var light = (1 - (0.299 * r + 0.587 * g + 0.114 * b) / 255 < 0.5)

        if(!light) {
            self.buttonOutproject.addClass('dark-bg')
        }else {
            self.buttonOutproject.removeClass('dark-bg')
        }
    }

    function initEvents() {
        self.window.on('scroll', function(event){
            self.lastY = self.window.scrollTop();

            sectionsInView();
        }).on('resize', function() {
            sections();
        });

        self.buttonOutproject.on('click', function() {
            self.openOutproject = !self.openOutproject;

            stateOutproject()

            return false;
        })
    }

    self.init = function() {
        if(!self.container.length) return false;

        initEvents();

        setTimeout(function() {
            sections();
        }, 0)
    }
}

$(document).ready(function() {
    var panel = new Panel({});

    panel.init();
})