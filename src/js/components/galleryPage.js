function GalleryPage(options) {
    var self = this;

    self.window = $(window);

    self.options = {};

    self.container = $('.gallery-content-carousel');
    self.containerMenu = $('.gallery-page_menu');

    self.items = $('.gallery-content-carousel_item');

    self.itemMenu = self.containerMenu.find('li');
    self.buttonMenu = self.containerMenu.find('a');

    self.viewPage = 1;

    self.currentClass = 'current';

    function state() {
        self.items.removeClass(self.currentClass)
            .eq(self.viewPage-1).addClass(self.currentClass)
    }

    function initEvents() {
        self.itemMenu.find('a').on('click', function() {
            var item = $(this);
            self.viewPage = item.data('index');
            self.itemMenu.removeClass(self.currentClass)
            item.parent().addClass(self.currentClass)

            state()

            return false;
        })
    }

    self.init = function() {
        if(!self.container.length) return false;

        initEvents();
    }
}

$(document).ready(function() {
    var galleryPage = new GalleryPage({});

    galleryPage.init();
})