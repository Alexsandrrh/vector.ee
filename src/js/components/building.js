function BuildingModel(options) {
    var el = document.getElementById('buildingmodel');
    if(!el) return false;
    if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

    var container, stats, controls;
    container = document.getElementById( 'webgl');
    var heading = document.querySelector('.main-title');
    var title = document.querySelector('.building-title');
    container.style.opacity = 0;
    var camera, scene, renderer;

    var progress = document.createElement('div');
    var progressBar = document.createElement('div');
    progress.classList.add('progress-load');
    progressBar.classList.add('progress-bar');

    progress.appendChild(progressBar);

    title.appendChild(progress);

    function loadBar() {
        TweenMax.to(
            progressBar, .75, {
                width: '100%'
            }
        );
    }
    requestAnimationFrame(loadBar);

    var mouseX = 0, mouseY = 0;
    var windowHalfX = window.innerWidth / 2;
    var windowHalfY = window.innerHeight / 2;

    renderer = new THREE.WebGLRenderer( { antialias: true, powerPreference:"high-performance" } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );

    camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 100000 );
    camera.position.set( 400, 400, 2500 ); // x z y

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x080E1D );

    var lightA = new THREE.AmbientLight( 0xffffff ); // soft white light
    scene.add( lightA );

    function init() {
        el.appendChild( container );
        container.appendChild( renderer.domElement );
        window.addEventListener( 'resize', onWindowResize, false );
    };

    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'scroll', onMouseScroll, false );

    function onDocumentMouseMove( event ) {
        mouseX = ( event.clientX - windowHalfX );
        mouseY = ( event.clientY - windowHalfY );
    }

    var manager = new THREE.LoadingManager();
    manager.onStart = function ( url, itemsLoaded, itemsTotal ) {
        loadBar();
    };

    manager.onLoad = function ( ) {
        title.removeChild(progress);
    };

    manager.onError = function ( url ) {
        console.error( 'There was an error loading ' + url );
    };

    var loader = new THREE.FBXLoader(manager);
    loader.load( 'models/vector-test-10.fbx', function ( object ) {

        scene.add( object );

        var element = scene.getObjectByName('house');

        var material = new THREE.MeshPhongMaterial( {
            color: 0x080E1D,
            shading: THREE.FlatShading,
            polygonOffset: true,
            polygonOffsetFactor: 0, // positive value pushes polygon further away
            polygonOffsetUnits: 1
        } );

        element.material = material;

        var geo = new THREE.EdgesGeometry( element.geometry ); // or WireframeGeometry
        var mat = new THREE.LineBasicMaterial( { linewidth: 1, color: 0x4A5162 } );
        var wireframe = new THREE.LineSegments( geo, mat );

        wireframe.computeLineDistances();
        element.add( wireframe );

        function animateLine() {
            requestAnimationFrame( animateLine );

            if (mat.dashSize<10){
            }
        };
        animateLine();

        window.preloader = false;
        TweenMax.fromTo(
            heading, 1, {
                opacity: 0,
                transform: 'translateY(400%)'
            }, {
                opacity: 1,
                transform: 'translateY(0%)',
                ease: Power2.easeOut
            });
        TweenMax.fromTo(
            container, 2, {
                opacity: 0,
                transform: 'translateY(100%)'
            }, {
                opacity: 1,
                transform: 'translateY(0%)',
                ease: Power3.easeOut
            });
    } );

    function onWindowResize() {
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
    }

    var scrolled = 0;

    function onMouseScroll() {
        var width = window.innerWidth > 1200 ? window.innerWidth / 4 : 0;
        var height = window.innerHeight;

        scrolled = window.pageYOffset || document.documentElement.scrollTop;

        var ratioHeight = Math.ceil((scrolled * 100) / height);
        var offsetX = Math.ceil((width * ratioHeight) / 100);

        if(scrolled >= height) return false;

        container.setAttribute("style", `transform: translateY(${scrolled}px) translateX(-${offsetX}px);`);

    }

    function animate() {
        requestAnimationFrame( animate );
        render();
    }

    function render() {

        camera.position.x += ( mouseX - camera.position.x ) * 0.05;
        camera.position.y += ( - mouseY - camera.position.y + 500 + scrolled * 5 ) * 0.05;
        camera.lookAt( new THREE.Vector3(0,500,0) );
        renderer.render( scene, camera );
    }

    init();
    animate();
}

$(document).ready(function() {
    if(document.getElementById('buildingmodel')) {
        BuildingModel();
    }
})
