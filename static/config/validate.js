var constraints = {
    email: {
      presence: {
        message: "Поле обязательно для заполнения! "
      },
      email: {
        message: "Поле должно содержать e-mail! "
      }
    },
    name: {
      presence: {
        message: "Поле обязательно для заполнения! "
      },
      length: {
        minimum: 2,
        message: "Поле должно содержать минимум 2 символа! "
      },
      format: {
        pattern: "[a-zA-Zа-яА-Я]+",
        flags: "i",
        message: "Может содержать только буквы! "
      }
    },
    phone: {
        presence: {
          message: "Поле обязательно для заполнения! "
        }
    }
};